package com.jojajones.buttoncounterapp

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.View
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    //    private var userInput: EditText? = null
    private val TAG = "ActivityLifeCycle"
    private val TEXT_DATA = "TextView Contents"
    private val MAP_NUM_DATA = "Map Num Data"
    private val MAP_TEXT_DATA = "Map Text Data"
    private var button: Button? = null
    //    private var textView: TextView? = null
    private var clickCount = HashMap<String, Int>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //userInput = findViewById(R.id.userInput)
        button = findViewById(R.id.button)
//        textView = findViewById(R.id.textView)
        textView.movementMethod = ScrollingMovementMethod()
        Log.d(TAG, "onCreate called")
    }

    override fun onStart() {
        Log.d(TAG, "onStart called")
        super.onStart()
    }

    override fun onResume() {
        Log.d(TAG, "onResume called")
        super.onResume()
    }

    override fun onRestart() {
        Log.d(TAG, "onRestart called")
        super.onRestart()
    }

    override fun onPause() {
        Log.d(TAG, "onPause called")
        super.onPause()
    }

    override fun onStop() {
        Log.d(TAG, "onStop called")
        super.onStop()
    }

    override fun onDestroy() {
        Log.d(TAG, "onDestroy caled")
        super.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        Log.d(TAG, "onSaveInstanceState called")
        super.onSaveInstanceState(outState)
        outState?.putString(TEXT_DATA, textView.text.toString())
        val keys = ArrayList<String>()
        val nums = ArrayList<Int>()

        for((key, value) in clickCount){
            keys.add(key)
            nums.add(value)
        }
        outState?.putIntegerArrayList(MAP_NUM_DATA, nums)
        outState?.putStringArrayList(MAP_TEXT_DATA, keys)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
        Log.d(TAG, "onRestoreInstanceState called")
        textView.text = savedInstanceState?.get(TEXT_DATA).toString()
        var tempKeyArray = savedInstanceState?.getStringArrayList(MAP_TEXT_DATA)
        var tempNumArray = savedInstanceState?.getIntegerArrayList(MAP_NUM_DATA)
        if (tempKeyArray != null && tempNumArray != null) {
            for(x in 0 until tempKeyArray.count()){
                clickCount[tempKeyArray[x]] = tempNumArray[x]
            }
        }
        super.onRestoreInstanceState(savedInstanceState)
    }

    fun onButtonClick(view: View?) {
        val userInputText = userInput.text.toString()
        if (clickCount[userInputText] == null) {
            clickCount[userInputText] = 1
        } else {
            clickCount[userInputText] = clickCount[userInputText]!! + 1
        }
        if (userInputText == "") {
            textView.append(
                "The button has been tapped ${clickCount[userInputText]} time${if (clickCount[userInputText] == 1) {
                    ""
                } else {
                    "s"
                }}\n"
            )
        } else {
            textView.append(
                "$userInputText has tapped the button ${clickCount[userInputText]} time${if (clickCount[userInputText] == 1) {
                    ""
                } else {
                    "s"
                }}\n"
            )
        }
    }
}
